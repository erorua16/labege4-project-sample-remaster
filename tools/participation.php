<?php



function get_participations($idproject){
    global $cnx;
    $sth = $cnx->prepare("SELECT * FROM participations WHERE idproject = :idprojet;");
    $sth->execute();
    return $sth->fetchAll();
}



function add_participation($idproject, $iduser){
    global $cnx;
    $sth = $cnx->prepare("INSERT INTO participations (FK_user, FK_project) VALUES (:iduser, :idproject);");
    return $sth->execute(["iduser"=>$iduser, "idproject"=>$idproject]);
}



function delete_participation($idproject, $iduser){
    global $cnx;
    $sth = $cnx->prepare("DELETE FROM participations WHERE FK_user = :iduser AND FK_project = :idproject;");
    return $sth->execute(["iduser"=>$iduser, "idproject"=>$idproject]);
}
